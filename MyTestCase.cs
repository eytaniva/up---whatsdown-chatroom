﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ItSE_182_13.Business
{
    [TestFixture]
    class MyTestCase
    {
        [TestCase]
        public void TestCheckValidityUser()
        {
            User us1 = new User("test", "99","1234");
            Assert.AreEqual(true, us1.CheckValidity());
            User us2 = new User("test2", "x17", "1234");
            Assert.AreEqual(false, us2.CheckValidity());
            User us3 = new User("יוסי", "55", "1234");
            Assert.AreEqual(false, us3.CheckValidity());
            User us4 = new User("", "33", "1234");
            Assert.AreEqual(false, us4.CheckValidity());
            User us5 = new User("^u9nhu-=", "13", "1234");
            Assert.AreEqual(false, us5.CheckValidity());
            User us6 = new User("ok", "", "1234");
            Assert.AreEqual(false, us6.CheckValidity());
            User us7 = new User("koko", "76#", "1234");
            Assert.AreEqual(false, us7.CheckValidity());
            User us8 = new User("test", "-99", "1234");
            Assert.AreEqual(false, us8.CheckValidity());
        }

        [TestCase]
        public void TestCheckValidityCM()
        {
            ChatManager cm = new ChatManager(new User("test", "99", "1234"));
            string testS0 = "";
            string testS1 = "BonJoviRules!!!-BonJoviRules!!!-BonJoviRules!!!-BonJoviRules!!!-BonJoviRules!!!" +
                "BonJoviRules!!!-BonJoviRules!!!-BonJoviRules!!!-BonJoviRules!!!-BonJoviRules!!!";// 150 
            string test2 = " Somthing that needed to be printed";
            Assert.AreEqual(true, cm.checkValidity(test2));
            Assert.AreEqual(false, cm.checkValidity(testS1));
            Assert.AreEqual(false, cm.checkValidity(testS0));
        }

        [TestCase]
        public void TestIsPartOfGroup()
        {
            User us0 = new User("Test0", "66", "1234");
            Assert.AreEqual(true, us0.isPartOfGroup("66"));
            Assert.AreEqual(true, us0.isPartOfGroup(us0.GetGID()));
            Assert.AreEqual(false, us0.isPartOfGroup("666"));
            Assert.AreEqual(false, us0.isPartOfGroup("6"));
            Assert.AreEqual(false, us0.isPartOfGroup(""));//empty
            Assert.AreEqual(false, us0.isPartOfGroup("  "));//same Length, but empty 
            Assert.AreEqual(false, us0.isPartOfGroup("Test0"));

            User us1 = new User("Test1", "55", "1234");
            Assert.AreEqual(false, us1.isPartOfGroup(us0.GetGID()));
            Assert.AreEqual(false, us1.isPartOfGroup(us0.GetUserName()));
        }

        [TestCase]
        public void TestMessagesFromCertainGroupId()
        {
            User us = new User("Test", "99", "1234");
            ChatManager cm = new ChatManager(us);
            List<ReadOnlyMessage> messages = cm.Get_Display_MSG_Messages(1, false, "", "");
            List<string> groups = new List<string>();

            foreach (ReadOnlyMessage msg in messages)
            {
                if (!groups.Contains(msg.getgroupID()))
                    groups.Add(msg.getgroupID());
            }

            foreach (string id in groups)
            {
                List<ReadOnlyMessage> filteredMessages = cm.getMessagesFromGroupID(id);
                foreach (ReadOnlyMessage msg in filteredMessages)
                {
                    Assert.AreEqual(id, msg.getgroupID());
                }
            }
        }

        [TestCase]
        public void TestUserEquals()
        {
            User us1 = new User("name1", "1", "1234");
            User us2 = new User("name1", "2", "1234");
            Assert.AreEqual(false, us1.Equals(us2));
            User us3 = new User("name2", "1", "1234");
            Assert.AreEqual(false, us1.Equals(us3));
            User us4 = new User("name2", "2", "1234");
            Assert.AreEqual(false, us1.Equals(us4));
            User us5 = new User("name1", "1", "1235");
            Assert.AreEqual(true, us1.Equals(us5));
            object us6 = new object();
            Assert.AreEqual(false, us1.Equals(us6));
        }

        [TestCase]
        public void TestMessagesFromCertainUser()
        {
            User us = new User("Test", "99", "1234");
            ChatManager cm = new ChatManager(us);
            List<ReadOnlyMessage> messages = cm.Get_Display_MSG_Messages(1,false,"","");
            List<User> users = new List<User>();
            foreach (ReadOnlyMessage msg in messages)
            {
                User tmp = new User(msg.getUserName(), msg.getgroupID(), "1234");
                if (!users.Contains(tmp))
                    users.Add(tmp);
            }
            foreach (User usr in users)
            {
                List<ReadOnlyMessage> filteredMessages = cm.getMessagesFromUser(usr);
                foreach (ReadOnlyMessage msg in filteredMessages)
                {
                    User author = new User(msg.getUserName(), msg.getgroupID(), "1234");
                    Assert.AreEqual(usr, author);
                }
            }
        }

        [TestCase]
        public void TestPasswordValidity()
        {
            Assert.AreEqual(true, User.CheckPasswordValidity("1234"));
            Assert.AreEqual(false, User.CheckPasswordValidity(""));
            Assert.AreEqual(false, User.CheckPasswordValidity("123"));
            Assert.AreEqual(true, User.CheckPasswordValidity("atgaer"));
            Assert.AreEqual(true, User.CheckPasswordValidity("ssssssssssssssss"));
            Assert.AreEqual(false, User.CheckPasswordValidity("ssssssssssssssss1"));
            Assert.AreEqual(false, User.CheckPasswordValidity("%asdss"));
        }
    }
}
