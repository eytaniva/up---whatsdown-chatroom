﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ItSE_182_13.Business;
using ConsoleApp3;

namespace Project.PresentationLayer
{
    /// <summary>
    /// The window where the user signs int to the chat room
    /// </summary>
    public partial class SignIn : Window
    {
        ObservableObject _main = new ObservableObject();
        private bool passwordFlag;
        private string salt = "1337";
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public SignIn()
        {
            InitializeComponent();
            this.DataContext = _main;
            log.Info("Opened: SignIn Window");
        }
        //Tries to sign in the user
        private void buttonSignIn_Click(object sender, RoutedEventArgs e)
        {
            if (passwordFlag)
            {
                try
                {
                    log.Info("buttonSignIn_Click");
                    bool nextWindow = _main.SignInUser(_main.UserName, _main.GID, _main.Password);
                    if (nextWindow)
                    {
                        MessageWindow m = new MessageWindow();
                        m.Show();
                        this.Close();
                        log.Debug("showing message window");
                    }
                    else
                    {
                        MessageBox.Show("User does not exist", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                catch (Exception E)
                {
                    log.Error("Error when signing in", E);
                }
            }
            else
            {
                MessageBox.Show("Illegal password", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                log.Error("Illegal password was entered");
            }
        }
        //Sends user to registration screen
        private void buttonRegister_Click(object sender, RoutedEventArgs e)
        {
            Registration r = new Registration();
            r.Show();
            this.Close();
        }
        //Exits program
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            log.Info("CLOSE PROGRAM");
            this.Close();
        }

        private void passwordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            PasswordBox pb = sender as PasswordBox;
            if (User.CheckPasswordValidity(pb.Password))
            {
                _main.Password = Hashing.GetHashString(pb.Password+salt);
                passwordFlag = true;
            }
            else
                passwordFlag = false;
        }
    }
}
