﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using ItSE_182_13.Business;
using Project.PresentationLayer;
using System.Windows;

namespace Project.PresentationLayer
{
    class GuiManager
    {
        //Fields
        public static ChatManager ChatManagerInstance = new ChatManager(null);
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //constractor
        public GuiManager()
        {
            log.Info("GuiManager started");
            MainWindow m = new MainWindow();
            m.Show();

        }
    }
}
