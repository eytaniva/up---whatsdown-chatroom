﻿using ItSE_182_13.Business;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Linq;
using System.Windows.Data;
using System;
using MileStoneClient.CommunicationLayer;
using System.Windows;
using System.Windows.Controls;
using ConsoleApp3;
using Project.DataAccessLayer;
using ItSE_182_13.PresistentLayer;

namespace Project.PresentationLayer
{

    public class ObservableObject : INotifyPropertyChanged
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public event PropertyChangedEventHandler PropertyChanged;
        private int filter = 1;
        public ObservableCollection<ReadOnlyMessage> Messages { get; set; } = new ObservableCollection<ReadOnlyMessage>();
        public ObservableCollection<ReadOnlyMessage> prevMessages { get; set; } = new ObservableCollection<ReadOnlyMessage>();
        public ObservableObject()
        {
            Messages.CollectionChanged += Messages_CollectionChanged;
        }

        private void Messages_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            OnPropertyChanged("Messages");
        }
        //Asks ChatManager to send a message
        public ReadOnlyMessage SendMessage()
        {
            log.Info("in observable object - SendMessage");
            log.Debug("message content here is1: " + messageContent);
            Message m = GuiManager.ChatManagerInstance.Send(messageContent);
            ReadOnlyMessage im = m.ConvertToReadOnlyMessage();
            while (Messages.Count() > 200)
            {
                log.Debug("too many messages");
                ReadOnlyMessage oldest = FindOldest();
                Messages.Remove(oldest);
            }
            if (im != null)
                return im;
            else
                return null;
        }
        //Checks with ChatManager if there are any new messages, and if there are then
        //the method displays them and returns true, also removed oldest messages if there are more
        // than 200 messages displayed
        public bool UpdateMessages(bool firstTime)
        {
            log.Info("Updating Messages");
            bool added = false;
            List<ReadOnlyMessage> newMessages = GuiManager.ChatManagerInstance.Get_Display_MSG_Messages(filter, firstTime, filterUserName, filterGroupId);
            foreach (ReadOnlyMessage msg in newMessages)
            {
                if (!Messages.Contains(msg))
                {
                    if (filterNone)
                    {
                        Messages.Insert(0,msg);
                        prevMessages.Insert(0,msg);
                        added = true;
                    }
                    else
                    {
                        if (filterGroup)
                        {
                            if (msg.getgroupID().Equals(filterGroupId))
                            {
                                Messages.Insert(0, msg);
                                prevMessages.Insert(0, msg);
                                added = true;
                            }
                        }
                        else
                        {
                            if (msg.getgroupID().Equals(filterGroupId) & msg.getUserName().Equals(filterUserName))
                            {
                                Messages.Insert(0, msg);
                                prevMessages.Insert(0, msg);
                                added = true;
                            }
                        }
                    }
                }
            }
            while(Messages.Count > 200)
            {
                ReadOnlyMessage oldest = FindOldest();
                Messages.Remove(oldest);
            }
            MessageHandler.updateLatestMsgPull(prevMessages.Last().getDate());
            return added;
        }
        //Returns a pointer to the oldest message in the collection
        private ReadOnlyMessage FindOldest()
        {
            ReadOnlyMessage oldest = Messages.ElementAt(0);
            foreach(ReadOnlyMessage rom in Messages)
            {
                if (rom.getDate() < oldest.getDate())
                    oldest = rom;
            }
            return oldest;
        }
        #region Binding
        private string messageContent = "";
        private string userName = "";
        private string gid = "";
        private bool sortByTime = true;
        private bool sortByUser = false;
        private bool sortByTimeAndUser = false;
        private bool direction = true;
        private bool filterNone = true;
        private bool filterGroup = false;
        private bool filterUser = false;
        private bool filterSomething = false;
        private string filterGroupId = "";
        private string filterUserName = "";
        private string password = "";
        private int messageIndex = 0;
        private string editedContent = "";
        //Bound to the text field that sends messages
        public string MessageContent
        {
            get
            {
                return messageContent;
            }
            set
            {
                log.Debug("changing message content to: " + value);
                messageContent = value;
                OnPropertyChanged("MessageContent");
            }
        }
        //Bound to the text field that recieves username (on signup and registration)
        public string UserName
        {
            get
            {
                return userName;
            }
            set
            {
                userName = value;
                OnPropertyChanged("UserName");
            }
        }
        //Bound to the text field that recieves Group Id (on signup and registration)
        public string GID
        {
            get
            {
                return gid;
            }
            set
            {
                gid = value;
                OnPropertyChanged("GID");
            }
        }
        //Bound to the radio button that sorts by time
        public bool SortByTime
        {
            get
            {
                return sortByTime;
            }
            set
            {
                sortByTime = value;
                OnPropertyChanged("SortByTime");
            }
        }
        //Bound to the radio button that sorts by user
        public bool SortByUser
        {
            get
            {
                return sortByUser;
            }
            set
            {
                sortByUser = value;
                OnPropertyChanged("SortByUser");
            }
        }
        //Bound to the radio button that sorts by time and user
        public bool SortByTimeAndUser
        {
            get
            {
                return sortByTimeAndUser;
            }
            set
            {
                sortByTimeAndUser = value;
                OnPropertyChanged("SortByTimeAndUser");
            }
        }
        //Bound to the combobox that chooses the direction of the sort
        public bool Direction
        {
            get
            {
                return direction;
            }
            set
            {
                direction = value;
                OnPropertyChanged("Direction");
            }
        }
        //Bound to the radio button that filters by none
        public bool FilterNone
        {
            get
            {
                return filterNone;
            }
            set
            {
                filterNone = value;
                FilterSomething = !value;
                OnPropertyChanged("FilterNone");
                log.Debug("filter none is: " + filterNone);
            }
        }
        //Bound to the radio button that filters by Group ID
        public bool FilterGroup
        {
            get
            {
                return filterGroup;
            }
            set
            {
                filterGroup = value;
                //log.Debug("filter none is: " + filterNone);
                OnPropertyChanged("FilterGroup");
            }
        }
        //Bound to the radio button that filters by User
        public bool FilterUser
        {
            get
            {
                return filterUser;
            }
            set
            {
                filterUser = value;
                //log.Debug("filter none is: " + filterNone);
                OnPropertyChanged("FilterUser");
            }
        }
        //Bound to the value of IsEnabled of the group id text field
        public bool FilterSomething
        {
            get
            {
                return filterSomething;
            }
            set
            {
                filterSomething = value;
                log.Debug("VISIBILITY IS " + filterSomething);
                OnPropertyChanged("FilterSomething");
            }
        }
        //Bound to the text field that represents the group id to filter
        public string FilterGroupId
        {
            get
            {
                return filterGroupId;
            }
            set
            {
                filterGroupId = value;
                OnPropertyChanged("FilterGroupId");
            }
        }
        //Bound to the text field that represents the username to filter
        public string FilterUserName
        {
            get
            {
                return filterUserName;
            }
            set
            {
                filterUserName = value;
                OnPropertyChanged("FilterUserName");
            }
        }
        //Bound to the password field
        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
                OnPropertyChanged("Password");
            }
        }
        //Bound to the index of the selected message in the list box
        public int MessageIndex
        {
            get
            {
                return messageIndex;
            }
            set
            {
                messageIndex = value;
                OnPropertyChanged("MessageIndex");
            }
        }
        //Bound to the text field of the edited content
        public string EditedContent
        {
            get
            {
                return editedContent;
            }
            set
            {
                editedContent = value;
                OnPropertyChanged("EditedContent");
            }
        }
        #endregion

        //Asks ChatManager to sign up a user
        public bool RegisterUser(string nn, string groupID, string hashedPassword)
        {
            log.Info("In ObservableObject - RegisterUser");
            User us = new User(nn, groupID, hashedPassword);
            if (!us.CheckValidity())
                throw new Exception("Illegal User name or Group ID");
            if (GuiManager.ChatManagerInstance.IsRegistered(us))
                return false;
            GuiManager.ChatManagerInstance.RegisterUser(us);
            return true;
        }
        //Asks ChatManager to sign in a user
        public bool SignInUser(string nn, string groupID, string hashedPassword)
        {
            log.Info("In ObservableObject - SignInUser");
            User us = new User(nn, groupID, hashedPassword);
            bool userExists = GuiManager.ChatManagerInstance.IsSignedIn(us);
            if(userExists)
            {
                GuiManager.ChatManagerInstance.SignIn(us);
                return true;
            }
            return false;
        }
        //Filters the displayed messages according to other variables
        public void FilterBy()
        {
            int prev = filter;
            bool legal = true;
            log.Info("In ObservableObject - FilterBy");
            if (FilterNone)
            {
                filter = 1;
            }
            if (filterGroup)
            {
                filter = 2;
                int tmp;
                if (!int.TryParse(filterGroupId, out tmp))
                    legal = false;
            }
            if (filterUser)
            {
                filter = 3;
                User tmp = new User(FilterUserName, filterGroupId, "1234");
                legal = tmp.CheckValidity();
            }
            Messages.Clear();
            if (legal)
                UpdateMessages(true);
            else
            {
                filter = prev;
                UpdateMessages(true);
                throw new Exception();
            }
            SortBy();
        }
        //Sorts the displayed messages according to other variables
        public void SortBy()
        {
            log.Info("In ObservableObject - SortBy");
            log.Debug("direction is: " + direction);
            ObservableCollection<ReadOnlyMessage> sortedMessages = null;
            if (sortByTime)
            {
                log.Debug("time");
                sortedMessages = new ObservableCollection<ReadOnlyMessage>(Messages.OrderBy(x => x.getDate()));
                if (!direction)
                    sortedMessages.Reverse();
                UpdateGUI(sortedMessages);
            }
            if(sortByUser)
            {
                log.Debug("user");
                sortedMessages = new ObservableCollection<ReadOnlyMessage>(Messages.OrderBy(x => x.getUserName()));
            }
            if(sortByTimeAndUser)
            {
                sortedMessages = new ObservableCollection<ReadOnlyMessage>(Messages.OrderBy(x => int.Parse(x.getgroupID())).ThenBy(y => y.getUserName()).ThenBy(z => z.getDate()));
            }
            if (sortedMessages != null)
            {
                if (!direction)
                {
                    sortedMessages = new ObservableCollection<ReadOnlyMessage>(sortedMessages.Reverse());
                }
                UpdateGUI(sortedMessages);
            }
        }
        //Tries to edit a message
        public bool EditMessage()
        {
            ReadOnlyMessage removed = Messages.ElementAt(messageIndex);
            User signedin = GuiManager.ChatManagerInstance.getSignedInUser();
            if (!GuiManager.ChatManagerInstance.checkValidity(editedContent))
                throw new Exception();
            else
            {
                if (signedin != null && (signedin.GetUserName().Equals(removed.getUserName()) & signedin.GetGID().Equals(removed.getgroupID())))
                {
                    Messages.RemoveAt(messageIndex);
                    log.Debug("removed: " + removed.ToString());
                    Message editedMessage = removed.ConvertToMessage();
                    GuiManager.ChatManagerInstance.EditMessage(editedMessage, editedContent);
                    Messages.Add(editedMessage.ConvertToReadOnlyMessage());
                    return true;
                }
                else
                    return false;
            }
        }
        //Updates the GUI
        private void UpdateGUI(ObservableCollection<ReadOnlyMessage> sortedMessages)
        {
            log.Info("In ObservableObject - UpdateGUI");
            Messages.Clear();
            foreach (ReadOnlyMessage msg in sortedMessages)
            {
                Messages.Add(msg);
            }
        }
        //Asks ChatManager to log out the user
        public void logOut()
        {
            log.Info("In ObservableObject - logOut");
            GuiManager.ChatManagerInstance.Signout();
            
        }
        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
