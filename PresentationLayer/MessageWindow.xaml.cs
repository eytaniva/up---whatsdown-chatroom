﻿using ItSE_182_13.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Project.PresentationLayer
{

    public partial class MessageWindow : Window
    {
        private static Random random = new Random();
        DispatcherTimer dispatcherTimer = new DispatcherTimer();
        ObservableObject _main = new ObservableObject();
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public MessageWindow()
        {
            InitializeComponent();
            this.DataContext = _main;
            log.Info("Enterd messages window");
            _main.UpdateMessages(true);
            dispatcherTimer.Tick += dispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 2, 0);
            dispatcherTimer.Start();

        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            try {
                bool added = _main.UpdateMessages(false);
                if (added)
                {
                    log.Debug("Added new Messages");
                    _main.SortBy();
                }
            }
            catch(Exception E)
            {
                log.Error("Error on timer tick", E);
            }
        }
        //Sends a message by pressing 'enter'
        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            BindingExpression be = TextField.GetBindingExpression(TextBox.TextProperty);
            be.UpdateSource();
            if (e.Key == Key.Enter)
            {
                btn_send_Click(sender, e);
            }
        }
        //Tries to send a message
        private void btn_send_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BindingExpression be = TextField.GetBindingExpression(TextBox.TextProperty);
                be.UpdateSource();
                ReadOnlyMessage toAdd = _main.SendMessage();
                _main.Messages.Add(toAdd);
                _main.prevMessages.Add(toAdd);
                _main.SortBy();
                _main.MessageContent = "";
            }
            catch
            {
                MessageBox.Show("Illegal Message", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }
        //Sorts the displayed messages
        private void buttonSort_Click(object sender, RoutedEventArgs e)
        {
            log.Debug("Sort");
            try {
                _main.SortBy();
            }
            catch(Exception E)
            {
                log.Error("Error when trying to sort", E);
            }
        }
        //Filters the displayed messages
        private void buttonFilter_Click(object sender, RoutedEventArgs e)
        {
            log.Debug("Filter");
            try {
                _main.FilterBy();
            }
            catch(Exception E)
            {
                MessageBox.Show("Illegal input - Filter was not applied", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                log.Error("Illegal input", E);
            }
        }
        //Exits the program
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            log.Info("CLOSE PROGRAM");
            Close();
        }
        //Logs the user out
        private void buttonLogOut1_Click(object sender, RoutedEventArgs e)
        {
            log.Info("Log out");
            try {
                _main.logOut();
                MainWindow m = new MainWindow();
                dispatcherTimer.Stop();
                this.Close();
                m.Show();
            }
            catch(Exception E)
            {
                log.Error("Error when logging out", E);
            }
        }
        //Edits a message
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try {
                if (_main.EditMessage())
                {
                    log.Debug("removed message");
                    _main.EditedContent = "";
                    _main.SortBy();
                }
                else
                {
                    MessageBox.Show("You can only edit your own messages", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                    log.Debug("cannot remove message");
                }
            }
            catch(Exception E)
            {
                log.Error("illegal message content", E);
                MessageBox.Show("Illegal message", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

    }
}
