﻿using ItSE_182_13.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Project.PresentationLayer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    //private GuiManager gm;

    public partial class MessageWindow : Window
    {
        private static Random random = new Random();
        DispatcherTimer dispatcherTimer = new DispatcherTimer();
        ObservableObject _main = new ObservableObject();
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public MessageWindow()
        {
            InitializeComponent();
            this.DataContext = _main;
            log.Info("Enterd messages window");
            _main.UpdateMessages();
            dispatcherTimer.Tick += dispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 2, 0);
            dispatcherTimer.Start();

        }

        private string RandomString(int length)
        {
            const string chars = "AaBbCcDdEeFfGgHhIiJjKkLlMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            bool added = _main.UpdateMessages();
            if (added)
            {
                _main.SortBy();
                _main.FilterBy();
            }
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            BindingExpression be = TextField.GetBindingExpression(TextBox.TextProperty);
            be.UpdateSource();
            if (e.Key == Key.Enter)
            {
                btn_send_Click(sender, e);
            }
        }

        private void btn_send_Click(object sender, RoutedEventArgs e)
        {
<<<<<<< HEAD
            try
            {
                ReadOnlyMessage toAdd = _main.SendMessage();
                _main.Messages.Add(toAdd);
                _main.MessageContent = "";
            }
            catch(Exception E)
            {
                log.Error("CANT SENT THE MESSAGE",E);
            }
=======
            ReadOnlyMessage toAdd = _main.SendMessage();
            _main.Messages.Add(toAdd);
            _main.prevMessages.Add(toAdd);
            _main.SortBy();
            _main.FilterBy();
            _main.MessageContent = "";
>>>>>>> Filtering
        }

        private void buttonSort_Click(object sender, RoutedEventArgs e)
        {
            log.Debug("button pressed");
            _main.SortBy();
        }

        private void buttonFilter_Click(object sender, RoutedEventArgs e)
        {
            _main.FilterBy();
        }
<<<<<<< HEAD
=======

        /*
private void StartOrStopRndChat(object sender, RoutedEventArgs e)
{
if (dispatcherTimer.IsEnabled)
dispatcherTimer.Stop();
else
dispatcherTimer.Start();          
}*/
>>>>>>> Filtering

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            log.Info("CLOSE PROGRAM");
            Close();
        }

        private void buttonLogOut1_Click(object sender, RoutedEventArgs e)
        {
            log.Info("Log out");
            _main.logOut();
            this.Close();
            MainWindow m =new MainWindow();
            m.Show();
        }
    }
}
