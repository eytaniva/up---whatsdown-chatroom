﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ItSE_182_13.Business;
using Project.PresentationLayer;
using System.Windows.Interop;

namespace Project
{
    /// <summary>
    /// The main window of the program, the user chooses if to signin, register or exit
    /// </summary>
    public partial class MainWindow : Window
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public MainWindow()
        {
            InitializeComponent();
            log.Info("Opened: MainWindow Window");
        }
        //Sends user to sign in window
        private void buttonSignIn_Click(object sender, RoutedEventArgs e)
        {
            log.Info("buttonSignIn_Click");
            SignIn si = new SignIn();
            si.Show();
            this.Close();
        }
        //Sends user to registration window
        private void buttonRegestration_Click(object sender, RoutedEventArgs e)
        {
            log.Info("buttonRegestration_Click");
            Registration r = new Registration();
            r.Show();
            this.Close();
        }
        //Exits program
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            log.Info("CLOSE PROGRAM");
            this.Close();
        }
    }
}
