﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ItSE_182_13.Business;
using ConsoleApp3;

namespace Project.PresentationLayer
{
    /// <summary>
    /// The window where the user registers
    /// </summary>
    public partial class Registration : Window
    {
        ObservableObject _main = new ObservableObject();
        private bool passwordFlag;
        private string salt = "1337";
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public Registration()
        {
            InitializeComponent();
            this.DataContext = _main;
            log.Info("Opened: Registration Window");
        }
        //Tries to register the user
        private void buttonReg_Click(object sender, RoutedEventArgs e)
        {
            log.Info("buttonReg_Click");
            if (passwordFlag)
            {
                try
                {
                    bool nextWindow = _main.RegisterUser(_main.UserName, _main.GID, _main.Password);
                    if (nextWindow)
                    {
                        MessageWindow m = new MessageWindow();
                        m.Show();
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("User already exists", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                        log.Error("User already exists");
                    }
                }
                catch(Exception E)
                {
                    MessageBox.Show("Illegal user name or group id", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                    log.Error("Illegal user name or group id",E);
                }
            }
            else
            {
                MessageBox.Show("Illegal password", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                log.Error("Illegal password was entered");
            }
        }
        //Exits the program
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            log.Info("CLOSE PROGRAM");
            this.Close();
        }
        //Sends the user to the signin screen
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            SignIn si = new SignIn();
            this.Close();
            si.Show();
        }

        private void passwordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            PasswordBox pb = sender as PasswordBox;
            if (User.CheckPasswordValidity(pb.Password))
            {
                _main.Password = Hashing.GetHashString(pb.Password + salt);
                passwordFlag = true;
            }
            else
                passwordFlag = false;
        }

        
    }
}
