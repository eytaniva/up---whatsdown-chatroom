﻿
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ItSE_182_13.PresistentLayer;
using MileStoneClient.CommunicationLayer;

namespace ItSE_182_13.Business
{
    [Serializable]
    public class Message : IMessage
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
                (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //fields
        private static PresistentLayer.MessageHandler messageHandler = new PresistentLayer.MessageHandler();
        private string content;
        private string userName;
        private Guid id;
        private string groupID;
        private DateTime date;

        Guid IMessage.Id
        {
            get
            {
                return this.id;
            }
        }

        string IMessage.UserName
        {
            get
            {
                return this.userName;
            }
        }

        DateTime IMessage.Date
        {
            get
            {
                return this.date;
            }
        }

        string IMessage.MessageContent
        {
            get
            {
                return this.content;
            }
        }

        string IMessage.GroupID
        {
            get
            {
                return this.groupID;
            }
        }
        
        public string getUserName()
        {
            return this.userName;
        }
        public string getGroupID()
        {
            return this.groupID;
        }
        public Guid getGuid()
        {
            return this.id;
        }
        public string getContent()
        {
            return this.content;
        }
        public DateTime GetDate()
        {
            return this.date;
        }
        //constructor
        public Message(IMessage m)
        {
            log.Info("In message constructor");
            this.content = m.MessageContent;
            this.date = m.Date;
            this.groupID = m.GroupID;
            this.id = m.Id;
            this.userName = m.UserName;
        }
        public Message(Guid id, string userName, string groupID, string content, DateTime date)
        {
            this.id = id;
            this.content = content;
            this.userName = userName;
            this.groupID = groupID;
            this.date = date;
        }
        //Converts a message to ReadOnlyMessage
        public ReadOnlyMessage ConvertToReadOnlyMessage()
        {
            ReadOnlyMessage m = new ReadOnlyMessage(id, userName, groupID, content, date);
            return m;
        }
        //Edits a message
        public void EditMessage(string newContent)
        {
            content = newContent;
            date = DateTime.Now;
        }
        public override string ToString()
        {
            return this.userName + "(" + this.groupID + ") Sent: " + "\n'" + this.content + "'\n" + "Sent At: " + this.date;
        }
        public override bool Equals(Object other)
        {
            return (other is Message) && this.getGuid().Equals(((Message)other).getGuid());
        }
    }
}
