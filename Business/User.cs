﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using MileStoneClient.CommunicationLayer;

namespace ItSE_182_13.Business
{
    [Serializable]
    public class User
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
                (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //fields
        private string nickname;
        private string groupID;
        private string hashedPassword;
       
        //constructor
        public User( string nickname, string groupID, string hashedPassword)
        {
            this.nickname = nickname;
            this.groupID = groupID;
            this.hashedPassword = hashedPassword;
        }

        //methods
        //Checks the validity of the username and groupID
        public bool CheckValidity()
        {
            log.Info("In User - CheckValidity");
            int n;
            bool gid = int.TryParse(groupID, out n);
            if (gid)
            {
                int x = int.Parse(groupID);
                if (x <= 0)
                    gid = false;
            }
            bool nn = Regex.IsMatch(nickname, @"^[a-zA-Z0-9]+$") & nickname.Length>0 & nickname.Length <= 8;
            return gid&nn;
        }
        //Checks the validity of a password
        public static bool CheckPasswordValidity(string password)
        {
            if (password.Length < 4 | password.Length > 16)
                return false;
            foreach (char c in password)
            {
                if (!(c >= 'a' & c <= 'z') & !(c >= 'A' & c <= 'Z') & !(c >= '0' & c <= '9'))
                {
                    return false;
                }
            }
            return true;
        }
        public string GetUserName()
        {
            return this.nickname;
        }

        public string GetGID()
        {
            return this.groupID;
        }

        public string GetPassword()
        {
            return this.hashedPassword;
        }
        //Returns a string representation of a user
        public string toString()
        {
            return "User name: " + GetUserName() + ", GroupID: " + GetGID();
        }
        //Checks if a user is part of a certain group
        public bool isPartOfGroup(string groupID) {
            return this.groupID.Equals(groupID);
        }

        public bool Equals(User us) {
            if (this.GetUserName().Equals(us.GetUserName()) && this.GetGID() == us.GetGID())
                return true;
            else
                return false;
        }
    }
}
