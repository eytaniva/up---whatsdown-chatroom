﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Timers;
using System.Threading.Tasks;
using MileStoneClient.CommunicationLayer;
using ItSE_182_13.PresistentLayer;
using Project.DataAccessLayer;

namespace ItSE_182_13.Business
{
    [Serializable]
    class ChatManager
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
                (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //fields
        private User signInUser;
        private MessageHandler msgHandler = new MessageHandler();
        private UserHandler usHandler = new UserHandler();

        // static field
        private static int DISPLAY_MSG = 20;
        private static int MAX_MSG_LENGTH = 100;


        //constructor
        public ChatManager(User us)
        {
            SignIn(us);
        }

        //methods

        //Signs user in
        public bool SignIn(User us)
        {
            log.Info("In ChatManager - SignIn");
            this.signInUser = us;
            return true;
        }
        //Returns the signedin user
        public User getSignedInUser()
        {
            return this.signInUser;
        }
        //Gets a list of messages according to the filter
        public List<ReadOnlyMessage> Get_Display_MSG_Messages(int filter, bool firstTime, string userName, string groupID)
        {
            log.Info("In ChatManager - Get" + DISPLAY_MSG +" Messages");
            List<ReadOnlyMessage> messages = msgHandler.GetMessages(filter,firstTime,userName,groupID);
            return messages;
        }
        //Sends a message
        public Message Send(string messageContent)
        {
            log.Info("In ChatManager - Send");
            if (checkValidity(messageContent))
            {
                Message msg = new Message(Guid.NewGuid(), signInUser.GetUserName(), signInUser.GetGID(), messageContent, DateTime.Now);
                msgHandler.AddNewMessage(msg,signInUser);
                return msg;
            }
            else
            {
                throw new Exception("Illegal Message");
            }
        }
        //Returns all messages from a certain user
        public List<ReadOnlyMessage> getMessagesFromUser(User toShow)
        {
            log.Info("In ChatManager - getMessagesFromUser");
            List<Message> userMessages = new List<Message>();
            foreach(Message msg in userMessages.ToList())
            {
                if (msg.getGroupID() != toShow.GetGID() | msg.getUserName() != toShow.GetUserName())
                {
                    userMessages.Remove(msg);
                }
            }
            return ConvertListToReadOnly(userMessages);
        }
        //Returns all messages from a certain group
        public List<ReadOnlyMessage> getMessagesFromGroupID(string toShow)
        {
            log.Info("In ChatManager - getMessagesFromGID");
            List<Message> groupMessages = new List<Message>();
            //groupMessages = msgHandler.GetMessages();
            foreach (Message msg in groupMessages.ToList())
            {
                if (msg.getGroupID() != toShow)
                {
                    groupMessages.Remove(msg);
                }
            }

            return ConvertListToReadOnly(groupMessages);
        }
        //Registers a user
        public bool RegisterUser(User us)
        {
            log.Info("In ChatManager - RegisterUser");
            if (usHandler.AddNewUser(us))
            {
                signInUser = us;
                return true;
            }
            return false;
        }
        //Edits a certain message
        public void EditMessage(Message editedMessage, string EditedContent)
        {
            editedMessage.EditMessage(EditedContent);
            msgHandler.EditMessage(editedMessage);
        }
        //Checks whether or not a user is already registered
        public bool IsRegistered(User us)
        {
            log.Info("In ChatManager - IsRegistered");
            if (usHandler.UserExists(us))
                return true;
            return false;
        }
        //Checks if a details when a user signs in
        public bool IsSignedIn(User us)
        {
            log.Info("In ChatManager - IsRegistered");
            if (usHandler.IsCorrectUser(us))
                return true;
            return false;
        }
        //checks the validity of a message content input
        public bool checkValidity(string s1)
        {
            if ((s1.Length == 0) | (s1.Length >= (MAX_MSG_LENGTH + 1)))
            {
                log.Info("checkValidity - false");
                return false;
            }
            log.Info("checkValidity - true");
            return true;
        }
        //Signs out user
        public void Signout()
        {
            log.Info("In ChatManager - Signout");
            this.signInUser = null;
            Console.WriteLine("Signed out");
        }
        // return the logged in user
        public User getUser()
        {
            log.Info("In ChatManager - getUser");
            return this.signInUser;
        }
        //converts a list of messages to ReadOnlyMessages
        public List<ReadOnlyMessage> ConvertListToReadOnly(List<Message> ll)
        {
            log.Info("In ChatManager - ConvertToReadOnly");
            List<ReadOnlyMessage> ans = new List<ReadOnlyMessage>();
            foreach (Message m in ll)
            {
                ans.Add(new ReadOnlyMessage(m));
            }
            return ans;
        }
    }
}