﻿
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ItSE_182_13.PresistentLayer;
using MileStoneClient.CommunicationLayer;
using Project.PresentationLayer;
using ItSE_182_13.PresistentLayer;

namespace ItSE_182_13.Business
{
    public class ReadOnlyMessage
    {
        // loger;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //fields
        private static MessageHandler messageHandler = new MessageHandler();
        private string content;
        private string userName;
        private Guid id;
        private string groupID;
        private DateTime date;
       

        //constructor
        public ReadOnlyMessage(IMessage m)
        {
            //log.Info("In ReadOnlymessage constructor");
            this.content = m.MessageContent;
            this.date = m.Date;
            this.groupID = m.GroupID;
            if (this.groupID.Length == 1)
                this.groupID = "0" + this.groupID;
            this.id = m.Id;
            this.userName = m.UserName;
        }

        public ReadOnlyMessage(Guid id,string userName, string groupID, string content, DateTime date)
        {
            this.id = id;
            this.content = content;
            this.userName = userName;
            this.groupID = groupID;
            this.date = date;
        }


        //Methods
        public Message ConvertToMessage()
        {
            Message m = new Message(id, userName, groupID, content, date);
            return m;
        }
        public override string ToString()
        {
            return this.userName + "(" + this.groupID + ") Sent: " + "\n'" + this.content + "'\n" + "Sent At: " + this.date;
        }
        public string getUserName()
        {
            return this.userName;
        }
        public string getgroupID()
        {
            return this.groupID;
        }
        public string getContent()
        {
            return this.content;
        }
        public DateTime getDate()
        {
            return this.date;
        }
        public Guid getGUID()
        {
            return this.id;
        }
        public override bool Equals(Object other)
        {
            return (other is ReadOnlyMessage) && this.getGUID().Equals(((ReadOnlyMessage)other).getGUID());
        }
    }
}
