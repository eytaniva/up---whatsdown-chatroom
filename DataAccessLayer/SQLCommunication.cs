﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using ItSE_182_13.Business;
using System.Data;

namespace Project.DataAccessLayer
{
    public sealed class SQLCommunication
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
                (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        string connectionString = null;
        string sqlQuery = null;
        private static SQLCommunication instance = null;
        private static readonly object padlock = new object();
        string serverAddress = ""; // Enter the sever address here
        string databaseName = ""; // Enter the DB name here
        string userName = ""; //Enter the user name here
        string password = ""; //Enter the user's password here
        SqlConnection connection;
        SqlCommand command;
        SqlDataReader dataReader;

        /* IN THIS PROJECT I USE MY DB FIELDS. 
         * WHEN ENTERING A DB, PLEASE CHANGE THE FIELDS NAME TO MATCH YOUR DB. */

        private SQLCommunication()
        {
            connectionString = $"Data Source={serverAddress};Initial Catalog={databaseName};User ID={userName};Password={password}";
            connection = new SqlConnection(this.connectionString);
        }
        public static SQLCommunication Instance
        {
            get
            {   //only if there is no instance lock object, otherwise return instance
                if (instance == null)
                {
                    lock (padlock) // senario: n threads in here,
                    {              //locking the first and others going to sleep till the first get new Instance
                        if (instance == null)  // rest n-1 threads no need new instance because its not null anymore.
                        {
                            instance = new SQLCommunication();
                        }
                    }
                }
                return instance;
            }
        }

        //Returns a list of the last 200 mesages
        public Object GetNewMessages(DateTime LastDate, string query, int filter, int userID, string groupID, bool firstTime)
        {
            List<ReadOnlyMessage> messages = null;
            try
            {
                messages = new List<ReadOnlyMessage>();
                this.connection.Open();
                this.sqlQuery = query;
                this.command = new SqlCommand(this.sqlQuery, this.connection);
                SqlParameter last_date_param = new SqlParameter(@"lastdate", System.Data.SqlDbType.DateTime, 20);
                SqlParameter group_id_param = new SqlParameter(@"group_id", System.Data.SqlDbType.Int, 4);
                SqlParameter user_id_param = new SqlParameter(@"user_id", System.Data.SqlDbType.Int, 20);
                last_date_param.Value = LastDate.ToUniversalTime();
                if(groupID.Length != 0)
                    group_id_param.Value = int.Parse(groupID);
                if (userID != 0)
                    user_id_param.Value = userID;
                if (filter == 2) 
                    this.command.Parameters.Add(group_id_param);
                if (filter == 3)
                    this.command.Parameters.Add(user_id_param);
                if (!firstTime)
                    this.command.Parameters.Add(last_date_param);
                this.command.Prepare();
                this.dataReader = this.command.ExecuteReader();
                log.Debug("getting messages");
                while (this.dataReader.Read())
                {
                    Guid guid;
                    bool isValid = Guid.TryParse((string)this.dataReader.GetValue(0), out guid);
                    if (isValid)
                    {
                        string gid = this.dataReader.GetValue(2).ToString().Trim();
                        ReadOnlyMessage m = new ReadOnlyMessage(guid, ((string)this.dataReader.GetValue(1)).Trim(), this.dataReader.GetValue(2).ToString().Trim(), ((string)this.dataReader.GetValue(3)).Trim(), (DateTime.Parse(this.dataReader.GetValue(4).ToString())).ToLocalTime());
                        messages.Add(m);
                    }
                }
                this.dataReader.Close();
                this.command.Dispose();
                this.connection.Close();
                log.Debug("done getting messages");
            }
            catch (Exception e)
            {
                Console.WriteLine("Error with reading from the SQL");
                Console.WriteLine(e.ToString());
            }
            return messages;
        }
        //Edits a certain message in the database
        public void EditMessage(string content, DateTime date, Guid guid)
        {
            try
            {
                log.Debug("in ReadData SQL");
                this.connection.Open();
                this.command = new SqlCommand(null, this.connection);
                //this.command.CommandType = CommandType.Text;
                //string query = "UPDATE dbo.Messages SET Body = '" + content + "', SendTime = '" + date + "' WHERE Guid = '" + id + "';";
                this.command.CommandText =
                    "UPDATE dbo.Messages SET Body = @content, SendTime = @date WHERE Guid = @guid";
                SqlParameter content_param = new SqlParameter(@"content", System.Data.SqlDbType.Char, 100);
                SqlParameter date_param = new SqlParameter(@"date", System.Data.SqlDbType.DateTime, 20);
                SqlParameter guid_param = new SqlParameter(@"guid", System.Data.SqlDbType.Char, 68);

                content_param.Value = content;
                date_param.Value = date.ToUniversalTime();
                guid_param.Value = guid.ToString();
                this.command.Parameters.Add(content_param);
                this.command.Parameters.Add(date_param);
                this.command.Parameters.Add(guid_param);

                this.command.Prepare();
                this.command.ExecuteNonQuery();
                this.command.Dispose();
                this.connection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error with reading from the SQL");
                Console.WriteLine(e.ToString());
            }
        }
        //Checks if a similiar user is registered already
        public bool CheckIfUserExistsRegister(string nn, string gid)
        {
            bool userExists = false;
            try
            {
                this.connection.Open();
                this.command = new SqlCommand(null, this.connection);
                this.command.CommandText = "SELECT dbo.Users.Nickname, dbo.Users.Group_Id FROM dbo.Users WHERE dbo.Users.Nickname = @nickname AND dbo.Users.Group_Id = @group_id";

                SqlParameter group_id_param = new SqlParameter(@"group_id", SqlDbType.Int, 20);
                SqlParameter nickname_param = new SqlParameter(@"nickname", SqlDbType.Char, 20);

                group_id_param.Value = int.Parse(gid);
                nickname_param.Value = nn;

                this.command.Parameters.Add(nickname_param);
                this.command.Parameters.Add(group_id_param);

                this.command.Prepare();

                this.dataReader = this.command.ExecuteReader();
                userExists = this.dataReader.HasRows;
                this.dataReader.Close();
                this.command.Dispose();
                this.connection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error with reading from the SQL");
                Console.WriteLine(e.ToString());
            }
            return userExists;
        }
        //Checks if a certain user exists already
        public bool CheckIfUserExistsSignIn(string nn, string gid, string pass)
        {
            bool userExists = false;
            try
            {
                this.connection.Open();
                this.command = new SqlCommand(null, this.connection);
                this.command.CommandText = "SELECT * FROM dbo.Users WHERE Users.Nickname = @nickname AND Users.Group_Id = @group_id AND Users.Password = @password";

                SqlParameter group_id_param = new SqlParameter(@"group_id", SqlDbType.Int, 20);
                SqlParameter nickname_param = new SqlParameter(@"nickname", SqlDbType.Char, 20);
                SqlParameter password_param = new SqlParameter(@"password", SqlDbType.Char, 64);

                group_id_param.Value = int.Parse(gid);
                nickname_param.Value = nn;
                password_param.Value = pass;
                
                this.command.Parameters.Add(nickname_param);
                this.command.Parameters.Add(group_id_param);
                this.command.Parameters.Add(password_param);

                this.command.Prepare();

                this.dataReader = this.command.ExecuteReader();
                userExists = this.dataReader.HasRows;
                this.dataReader.Close();
                this.command.Dispose();
                this.connection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error with reading from the SQL");
                Console.WriteLine(e.ToString());
            }
            return userExists;
        }
        //Registers a user into the database
        public void RegisterUser(string nn, string gid, string pass)
        {
            try
            {
                this.connection.Open();
                this.command = new SqlCommand(null, this.connection);
                this.command.CommandText = "INSERT INTO dbo.Users ([Group_Id],[Nickname],[Password]) VALUES (@group_id, @nickname, @password)";

                SqlParameter group_id_param = new SqlParameter(@"group_id", SqlDbType.Int, 4);
                SqlParameter nickname_param = new SqlParameter(@"nickname", SqlDbType.Char, 20);
                SqlParameter password_param = new SqlParameter(@"password", SqlDbType.Char, 64);

                group_id_param.Value = int.Parse(gid);
                nickname_param.Value = nn;
                password_param.Value = pass;
                this.command.Parameters.Add(group_id_param);
                this.command.Parameters.Add(nickname_param);
                this.command.Parameters.Add(password_param);

                this.command.Prepare();
                this.command.ExecuteNonQuery();
                this.command.Dispose();
                this.connection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error");
                Console.WriteLine(ex.ToString());
            }
        }
        //Adds a new message to the database
        public void AddNewMSG(Guid guid, DateTime date, string content, int UserID)
        {

            try
            {
                this.connection.Open();
                this.command = new SqlCommand(null, this.connection);
                this.command.CommandText = "INSERT INTO dbo.Messages ([Guid], [User_Id], [SendTime], [Body]) VALUES (@guid, @userID, @time, @body)";

                SqlParameter userID_param = new SqlParameter(@"userID", System.Data.SqlDbType.Int, 20);
                SqlParameter guid_param = new SqlParameter(@"guid", System.Data.SqlDbType.Text, 68);
                SqlParameter time_param = new SqlParameter(@"time", System.Data.SqlDbType.DateTime, 20);
                SqlParameter body_param = new SqlParameter(@"body", System.Data.SqlDbType.Text, 100);
                

                userID_param.Value = UserID;
                guid_param.Value = guid.ToString();
                time_param.Value = date.ToUniversalTime();
                body_param.Value = content;

                this.command.Parameters.Add(guid_param);
                this.command.Parameters.Add(userID_param);
                this.command.Parameters.Add(time_param);
                this.command.Parameters.Add(body_param);

                this.command.Prepare();

                this.command.ExecuteNonQuery();
                this.command.Dispose();
                this.connection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error with writing to the SQL");
                Console.WriteLine(e.ToString());
            }
        }
        //Returns the userID of a user in the database
        public int GetUserID(string nn, string gid)
        {
            int output = 0;
            try
            {
                this.connection.Open();
                this.command = new SqlCommand(null, this.connection);
                this.command.CommandText = "SELECT TOP 1 dbo.Users.Id FROM dbo.Users WHERE dbo.Users.Nickname = @user_name AND dbo.Users.Group_Id = @group_id";

                SqlParameter user_name_param = new SqlParameter(@"user_name", System.Data.SqlDbType.Char, 20);
                SqlParameter group_id_param = new SqlParameter(@"group_id", System.Data.SqlDbType.Int, 20);

                user_name_param.Value = nn;
                group_id_param.Value = int.Parse(gid);

                this.command.Parameters.Add(user_name_param);
                this.command.Parameters.Add(group_id_param);

                this.dataReader = this.command.ExecuteReader();
                log.Debug("First Query is: " + this.sqlQuery);

                while (this.dataReader.Read())
                {
                    log.Debug("in loop");
                    output = ((int)this.dataReader.GetValue(0));
                }

                this.command.Dispose();
                this.connection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error with writing to the SQL");
                Console.WriteLine(e.ToString());
            }
            return output;
        }
    }
}
