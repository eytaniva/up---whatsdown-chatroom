﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MileStoneClient.CommunicationLayer;
using ItSE_182_13.Business;
using Project.DataAccessLayer;

namespace ItSE_182_13.PresistentLayer
{
    [Serializable]
    class MessageHandler
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
                (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static DateTime latestMessagePull = DateTime.Now;

        public static void updateLatestMsgPull(DateTime time)
        {
            latestMessagePull = time;
        }
        //Adds a new message to the database
        public void AddNewMessage(Message message, User us)
        {
            log.Info("In MessageHandler - AddNewMessage");
            int UserID = SQLCommunication.Instance.GetUserID(us.GetUserName(), us.GetGID());
            SQLCommunication.Instance.AddNewMSG(message.getGuid(), message.GetDate(), message.getContent(), UserID);
        }
        //Edits a message in the database
        public void EditMessage(Message message)
        {
            log.Debug("LATEST MESSAGE IS AT: " + latestMessagePull);
            SQLCommunication.Instance.EditMessage(message.getContent(), message.GetDate(), message.getGuid());
        }
        //Gets a list of messages according to the filter
        public List<ReadOnlyMessage> GetMessages(int filter, bool firstTime, string username, string groupid)
        {
            log.Info("In MessageHandler - GetMessages");
            int userId = 0;
            User tmp = new User(username, groupid, "1234");
            if(tmp.CheckValidity())
                userId = SQLCommunication.Instance.GetUserID(username, groupid);
            string query = "";
            switch (filter)
            {
                case 1: //no filters
                    if (firstTime)
                        query = "SELECT TOP 200 dbo.Messages.Guid, dbo.Users.Nickname, dbo.Users.Group_Id, dbo.Messages.Body, dbo.Messages.SendTime FROM dbo.Users INNER JOIN dbo.Messages ON dbo.Users.Id = dbo.Messages.User_Id ORDER BY dbo.Messages.SendTime DESC";
                    else
                        query = "SELECT TOP 200 dbo.Messages.Guid, dbo.Users.Nickname, dbo.Users.Group_Id, dbo.Messages.Body, dbo.Messages.SendTime FROM dbo.Users INNER JOIN dbo.Messages ON dbo.Users.Id = dbo.Messages.User_Id WHERE dbo.Messages.SendTime > @lastdate ORDER BY dbo.Messages.SendTime DESC";
                    break;
                case 2: //filter group
                    if (firstTime)
                        query = "SELECT TOP 200 dbo.Messages.Guid, dbo.Users.Nickname, dbo.Users.Group_Id, dbo.Messages.Body, dbo.Messages.SendTime FROM dbo.Users INNER JOIN dbo.Messages ON dbo.Users.Id = dbo.Messages.User_Id WHERE dbo.Users.Group_Id = @group_id ORDER BY dbo.Messages.SendTime DESC";
                    else
                        query = "SELECT TOP 200 dbo.Messages.Guid, dbo.Users.Nickname, dbo.Users.Group_Id, dbo.Messages.Body, dbo.Messages.SendTime FROM dbo.Users INNER JOIN dbo.Messages ON dbo.Users.Id = dbo.Messages.User_Id WHERE dbo.Users.Group_Id = @group_id AND dbo.Messages.SendTime > @lastdate ORDER BY dbo.Messages.SendTime DESC";
                    break;
                case 3: //filter user
                    if (firstTime)
                        query = "SELECT TOP 200 dbo.Messages.Guid, dbo.Users.Nickname, dbo.Users.Group_Id, dbo.Messages.Body, dbo.Messages.SendTime FROM dbo.Users INNER JOIN dbo.Messages ON dbo.Users.Id = dbo.Messages.User_Id WHERE dbo.Users.Id = @user_id ORDER BY dbo.Messages.SendTime DESC";
                    else
                        query = "SELECT TOP 200 dbo.Messages.Guid, dbo.Users.Nickname, dbo.Users.Group_Id, dbo.Messages.Body, dbo.Messages.SendTime FROM dbo.Users INNER JOIN dbo.Messages ON dbo.Users.Id = dbo.Messages.User_Id WHERE dbo.Users.Id = @user_id AND dbo.Messages.SendTime > @lastdate ORDER BY dbo.Messages.SendTime DESC";
                    break;
            }
            List<ReadOnlyMessage> messages = (List<ReadOnlyMessage>)SQLCommunication.Instance.GetNewMessages(latestMessagePull, query, filter, userId, groupid, firstTime);
            firstTime = false;
            return messages;
        }
    }
}
