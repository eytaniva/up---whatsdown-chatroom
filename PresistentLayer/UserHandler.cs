﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MileStoneClient.CommunicationLayer;
using ItSE_182_13.Business;
using Project.DataAccessLayer;
using System.Data.SqlClient;

namespace ItSE_182_13.PresistentLayer
{
    [Serializable]
    class UserHandler
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
                (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //Adds a new user to the database
        public bool AddNewUser(User us)//register
        {
            log.Info("In UserHandler - AddNewUser");
            SQLCommunication.Instance.RegisterUser(us.GetUserName(), us.GetGID(), us.GetPassword());
            return true;
        }
        public bool UserExists(User us)//checks only if there is a user with the given nickname and group id
        {
            log.Info("In UserHandler - UserExists Register");
            bool userExists = false;
            userExists = (bool)SQLCommunication.Instance.CheckIfUserExistsRegister(us.GetUserName(),us.GetGID());
            return  userExists;
        }

        public bool IsCorrectUser(User us)//checks if the username, groupId and password are correct
        {
            log.Info("In UserHandler - UserExists Signin");
            bool userExists = false;
            userExists = (bool)SQLCommunication.Instance.CheckIfUserExistsSignIn(us.GetUserName(), us.GetGID(), us.GetPassword());
            return userExists;
        }
    }
}
